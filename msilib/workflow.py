import os
import re
import csv
import sys
import natsort
import logging
from utilities import utils
from collections import defaultdict, namedtuple
from itertools import groupby
from operator import itemgetter
from numpy import std, array, ceil,  average


logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s - %(filename)s[line:%(lineno)d] - %(levelname)s: %(message)s')



## 1. do_format() --------------------------------------------------------------------------------------------------
def coords(row, chromosome=0, start=1, stop=2):
    return row[chromosome], int(row[start]), int(row[stop])


def msi_interval_creator(ranges):
    """Change the range information into varscan format
    chr start - T
    """
    data=[]
    #make sure the chr is sorted
    for chr in natsort.natsorted(ranges.keys()):
        #make sure the start position is sorted
        for row in sorted(ranges[chr]):
            info=(chr, row, '-', 'T')
            data.append(info)
    return data


def do_format(config):
    bedfile = config['bedfile']
    output = config['outfile']

    # prepare a dictionary of chromosome: set(positions)
    # includes all positions between start-stop
    ranges = defaultdict(set)
    writer = csv.writer(output, delimiter='\t')
    for row in csv.reader(bedfile, delimiter='\t'):
        chrom, beg, end = coords(row)
        ranges[chrom].update(range(beg - int(config['lextends'] * (end - beg)), end + 1))

    writer.writerows(msi_interval_creator(ranges))



## 2. do_counts() --------------------------------------------------------------------------------------------------

def parser_msi_bed(frc, row, msi_site, output_info):
    if not row['chrom'] in msi_site.keys():
        msi_site[row['chrom']] = {}

    for pos in range(int(int(row['start']) - (int(row['end']) - int(row['start'])) * frc), int(row['end']) + 1):
        msi_site[row['chrom']][pos] = row['start'] + '_' + row['end']

    for pos in range(int(row['start']), int(row['end']) + 1):
        msi_loci = row['chrom'] + '_' + str(pos)
        output_info[msi_loci] = {
            'chrom': row['chrom'],
            'position': pos,
            'ref_base': 'N',
            'depth': 0,
            'q10_depth': 0,
            'base:reads:strands:avg_qual:map_qual:plus_reads:minus_reads': 'N:0:0:0:0:0:0:0',
            'Misc': []
        }

    return msi_site, output_info


def calc_real_counts(detail, adjust_loci, output_info, stype=None):
    ### cacl region of extension
    length = adjust_loci - int(detail['position']) - 1

    ### cacl variation
    dels = filter(lambda s: 'DEL' in str(s), detail['Misc'])
    ins = filter(lambda s: 'INS' in str(s), detail['Misc'])
    sites = list(dels) + list(ins)

    for site in sites:
        info = re.split('[-:]', site)
        if info[0] == 'DEL' and int(info[1]) > length:
            if stype is not None:
                info[1] = length + 1
                info[2] = info[2][0:length + 1]
                if info[1] == 0:
                    continue
            else:
                info[1] = int(info[1]) - length
                info[2] = info[2][length:]

            output_info['Misc'].append(':'.join(
                ['-'.join([info[0], str(info[1]), info[2]]), info[3], info[4], info[5], info[6], info[7], info[8]]))

        if info[0] == 'INS' and length == 0 and len(set(info[2])) == 1 and stype is None:
            output_info['Misc'].append(site)

    ## update info
    output_info.update({
        'chrom': detail['chrom'],
        'position': detail['position'],
        'ref_base': detail['ref_base'],
        'depth': detail['depth'],
        'q10_depth': detail['q10_depth'],
        'base:reads:strands:avg_qual:map_qual:plus_reads:minus_reads': detail[
            'base:reads:strands:avg_qual:map_qual:plus_reads:minus_reads'],
    })

    return output_info



def do_counts(config):
    msi_site = {}
    output_info = {}

    ## 读取 bed 文件
    for row in csv.DictReader(config['bedfile'], fieldnames=['chrom', 'start', 'end', 'name'], restkey='Misc',
                              delimiter='\t'):
        msi_site, output_info = parser_msi_bed(config['lextends'], row, msi_site, output_info)

    # ## 读取readscount 文件并处理
    for row in csv.DictReader(config['countsfile'], delimiter='\t', restkey='Misc'):
        if int(row['position']) not in msi_site[row['chrom']].keys():
            continue

        start_loci = int(re.split(r'_', msi_site[row['chrom']][int(row['position'])])[0])
        end_loci = int(re.split(r'_', msi_site[row['chrom']][int(row['position'])])[1])

        if int(row['position']) <= start_loci:
            real_loci = row['chrom'] + '_' + str(start_loci)
            output_info[real_loci].update(calc_real_counts(row, start_loci, output_info[real_loci]))

        elif start_loci < int(row['position']) and int(row['position']) <= end_loci:
            real_loci = row['chrom'] + '_' + str(row['position'])
            output_info[real_loci].update(calc_real_counts(row, end_loci, output_info[real_loci], 1))

    fieldnames = ['chrom', 'position', 'ref_base', 'depth', 'q10_depth',
                  'base:reads:strands:avg_qual:map_qual:plus_reads:minus_reads']
    writer = csv.writer(config['outfile'], delimiter='\t')
    writer.writerow(fieldnames)

    for key, row in natsort.natsorted(output_info.items()):
        chrom_pos = re.split('_', key)
        output = [chrom_pos[0], chrom_pos[1], row['ref_base'], row['depth'], row['q10_depth'],
                  row['base:reads:strands:avg_qual:map_qual:plus_reads:minus_reads'], '0']
        if row['Misc']:
            for info in row['Misc']:
                output.append(info)
        writer.writerow(output)


#3. do_analyze -------------------------------------------------------------------------------------------------------
def calc_highest_peak(info, wt_fraction, average_depth):
    """
    Calculate the highest peak
    """
    highest_frac = wt_fraction
    for loci, details in info.items():
        details['allele_fraction'] = float(details['mutant_depth']) / average_depth
        if details['allele_fraction'] >= highest_frac:
            highest_frac = details['allele_fraction']

    return highest_frac


### 待确定
def define_sites(indels, sites):
    """
    Create a site entry for all variant lengths between abs(max) and 0
    """
    # If only one indel
    indels = list(indels)
    if len(indels) == 1:
        # If del
        if indels[0] > 0:
            mx = int(indels[0])
            mn = 0
        # If ins
        else:
            mx = 0
            mn = int(indels[0])
    else:
        mn = int(min(indels))
        mx = int(max(indels))
        # If both dels
        if mn < 0 and mx < 0:
            mx = 0
        # if both ins
        elif mn > 0 and mx > 0:
            mn = 0
    sites[0] = '0:0:0'
    for key in range(mn, mx + 1):
        if key not in sites.keys():
            sites[key] = str(key) + ':0:0'
    return sites


def calc_wildtype(indels, wildtype_ave_depth, wildtype_fraction, highest_frac):
    sites = {}
    # sites = define_sites(indels, sites)  ## 待确定

    try:
        wt_frac = float(wildtype_fraction) / highest_frac
    except ZeroDivisionError:
        wt_frac = float(wildtype_fraction)

    wildtype = ':'.join(['0', str(wt_frac), str(wildtype_ave_depth)])
    sites[0] = wildtype

    return sites


### 判断msi 的重复单元个数， 判断条件： 每个重复单元的占比（mutant_depth / avg depth）/ 最高重复单元的占比 >= 0.05
def calc_number_peaks(info, sites, highest_frac, cutoff):
    """
    Calculate the number of peaks that are above cutoff
    """
    peaks = 0
    for loci, details in info.items():
        ## 重要的方法学
        # take highest allele fraction and divide each alele fraction by that number
        try:
            allele_fraction = float(details['allele_fraction']) / highest_frac
        except ZeroDivisionError:
            allele_fraction = float(details['allele_fraction'])

        # Overwrite this site if the allele fraction is more than the cutoff
        sites[loci] = ':'.join([str(loci), str(allele_fraction), str(details['mutant_depth'])])

        if allele_fraction >= cutoff and int(details['mutant_depth']) >=3:
            peaks += 1

    # wildtype is sites[0], if fraction is above cutoff, it counts as a peak
    if float(sites[0].split(':')[1]) >= cutoff and int(sites[0].split(':')[2]) >=3:
        peaks += 1

    return peaks, sites


def calc_std_peaks(peaks):
    """
    Calculate the standard deviation of the alleles
    """
    std_peaks = []
    for peak in peaks:
        allele = peak.split(':')
        for i in range(0, int(allele[2])):
            std_peaks.append(int(allele[0]))

    a = array(std_peaks)
    stddev = format(std(a), '.6f')

    return stddev


def calc_summary_stats(output_info, cutoff):
    """Calculate average read depth, number of peaks, standard
    deviation and report each peak for each msi range in the bed file
    """
    sites = {}

    # msi_info is all loci for this chromosome
    for name, info in output_info.items():
        if info['total_depth'] != 0 and info['total_sites'] != 0:
            # Use ceil to round up
            average_depth = ceil(float(info['total_depth']) / info['total_sites'])
            # Turn to int
            average_depth = int(average_depth)
        else:
            average_depth = 0

        if average_depth != 0 and info['total_mutant_depth'] <= average_depth:
            wildtype_ave_depth = int(average_depth - info['total_mutant_depth'])
            wildtype_fraction = float(wildtype_ave_depth) / average_depth
        else:
            wildtype_ave_depth = 0
            wildtype_fraction = 0
            sites[0] = '0:0:0'

        if info['indels']:
            ## START MARK: Confirm the main repating cell type of the MSI site
            highest_frac = calc_highest_peak(info['indels'], wildtype_fraction, average_depth)
            sites = calc_wildtype(info['indels'].keys(), wildtype_ave_depth, wildtype_fraction, highest_frac)

            num_peaks, peaks = calc_number_peaks(info['indels'], sites, highest_frac, cutoff)
            stdev = calc_std_peaks(peaks.values())
            # Sort the peak list naturally (-3,-2,-1,0,1,2,3)
            peak_list = (" ").join(str(x) for x in natsort.natsorted(peaks.values()))

        elif average_depth != 0:
            # if there are no indels, but there are wild type reads
            wildtype_fraction = 1
            stdev = 0
            num_peaks = 1
            sites[0] = (':').join(['0', str(float(wildtype_fraction)), str(wildtype_ave_depth)])
            peak_list = sites[0]
        else:
            # if there are no reads at this site
            wildtype_fraction = 0
            sites[0] = (":").join(['0', str(float(wildtype_fraction)), str(wildtype_ave_depth)])
            num_peaks = 0
            peak_list = sites[0]
            stdev = 0

        output_info[name] = {'Name': info['Name'],
                             'Average_Depth': average_depth,
                             'Standard_Deviation': stdev,
                             'Number_of_Peaks': num_peaks,
                             'IndelLength:AlleleFraction:SupportingCalls': peak_list}
    return output_info


def calc_msi_dist(variant, msi_sites):
    """
    Compute statistics for each site in sample
    """
    # Want total depth to reflect all sites, not just DEL/INS
    msi_sites['total_depth'] = int(msi_sites['total_depth']) + int(variant['depth'])
    msi_sites['total_sites'] = int(msi_sites['total_sites']) + 1
    msi_sites['site_depth'] = int(variant['depth'])  ## record depth of last site

    # for variant in msi_sites:
    # info={}
    # reference/WT info:
    # wildtype_reads = variant['base:reads:strands:avg_qual:map_qual:plus_reads:minus_reads'].split(':')[1]

    # Grab all the deletions
    dels = filter(lambda s: 'DEL' in str(s), variant['Misc'])
    # And all the insertions
    ins = filter(lambda s: 'INS' in str(s), variant['Misc'])
    sites = list(dels) + list(ins)
    # pprint.pprint(sites)

    # Now process DEL/INS specific info
    # Parse the obvious variant format: 'DEL-3-AAA:1:1:23:1:1:0'
    for entry in sites:
        info = re.split('-|:', entry)
        if info[0] == 'DEL':
            length = int(info[1]) * int(-1)
        elif info[0] == 'INS':
            length = int(info[1])
        reads = info[3]

        # Want a total mutant depth for this loci
        msi_sites['total_mutant_depth'] += int(reads)
        msi_sites['mutant_tally'] += 1
        if length == 0:
            continue
        # If we haven't seen this length of mutation, add it
        if not length in msi_sites['indels'].keys():
            msi_sites['indels'][length] = {'allele_fraction': 0}
            msi_sites['indels'][length]['site_depth'] = int(variant['depth'])
            msi_sites['indels'][length]['mutant_depth'] = int(reads)
            msi_sites['indels'][length]['mutant_tally'] = 1
            # Otherwise, update the counts for this mutation length
        else:
            msi_sites['indels'][length]['site_depth'] += int(variant['depth'])
            msi_sites['indels'][length]['mutant_depth'] += int(reads)
            msi_sites['indels'][length]['mutant_tally'] += 1

    return msi_sites


def parse_msi_bedfile(row, msi_sites, output_info):
    """Create two dictionaries from bedfile
    one maps positions to name of loci
    one is output info for each loci
    """
    msi_loci = row['chrom'] + ':' + row['start'] + '-' + row['end']

    if not row['chrom'] in msi_sites.keys():
        msi_sites[row['chrom']] = {}

    for position in range(int(row['start']), int(row['end']) + 1):
        msi_sites[row['chrom']][position] = msi_loci

    output_info[msi_loci] = {'Name': row['name'],
                             'total_depth': 0,
                             'total_sites': 0,
                             'total_mutant_depth': 0,
                             'mutant_tally': 0,
                             'indels': {}}

    return (msi_sites, output_info)


def do_analyze(config):
    msifile = config['msi_file']
    bedfile = config['bedfile']
    outfile = config['outfile']
    cutoff = config['peak_fraction_cutoff']

    msi_sites = {}
    output_info = {}
    output = {}

    bedfile_handle = csv.DictReader(bedfile, delimiter='\t', fieldnames=['chrom', 'start', 'end', 'name'])
    for row in bedfile_handle:
        msi_sites, output_info = parse_msi_bedfile(dict(row), msi_sites, output_info)

    # now we start processing the sample msi info
    sample_msi = csv.DictReader(msifile, delimiter='\t', restkey='Misc')

    # Evaluate the sample-info vs msi-bed info, grouping on chromosome.
    for row in sample_msi:
        loci_name = msi_sites[row['chrom']][int(row['position'])]
        output_info[loci_name].update(calc_msi_dist(row, output_info[loci_name]))

    output.update(calc_summary_stats(output_info, cutoff))
    # with open(outfile, 'w') as csvfile:
    fieldnames = ['Position', 'Name', 'Average_Depth', 'Number_of_Peaks', 'Standard_Deviation',
                  'IndelLength:AlleleFraction:SupportingCalls']
    writer = csv.DictWriter(outfile, fieldnames=fieldnames, extrasaction='ignore', delimiter='\t')
    writer.writeheader()

    for key, row in natsort.natsorted(output.items()):
        writer.writerow(dict(row, **{'Position': key}))


#4. do_baseline() --------------------------------------------------------------------------------------

def do_baseline(config):
    path = config['path']
    outfile = config['outfile']

    control = defaultdict(list)

    # apply a series of filters to files
    files = filter(utils.msi_file_finder, utils.walker(path))
    files = sorted(files)

    for pth in files:
        with open(os.path.join(pth.dir, pth.fname)) as fname:
            reader = csv.DictReader(fname, delimiter='\t')
            info = sorted(reader, key=itemgetter('Position'))
            for k, g in groupby(info, key=itemgetter('Position')):
                for row in g:
                    if int(row['Average_Depth']) >= 30:
                        control[row['Position']].append(int(row['Number_of_Peaks']))

    header = ['Position', 'Standard_Deviation', 'Average_Number_Peaks', 'Count']
    writer = csv.writer(outfile, quoting=csv.QUOTE_MINIMAL, delimiter='\t')
    writer.writerow(header)
    for k, v in natsort.natsorted(control.items()):
        count = len(v)
        a = array(v)
        std_d = a.std()
        ave = average(a)
        row = [k, std_d, ave, count]
        if count >= 3:
            writer.writerow(row)


#4. do_summary() --------------------------------------------------------------------------------------
def msi_file_finder(pth):
    """
    Return True if pth represents a msi file file.
    """
    return bool(pth.fname.endswith('.msi.txt'))
    # return bool(re.search(r'.msi.txt', pth.fname))


def walker(dir):
    """Recursively traverse direcory `dir`. For each tuple containing
    (path, dirs, files) return a named tuple with attributes (dir,
    fname) for each file name in `files`.
    """
    Path = namedtuple('Path', ['dir', 'fname'])
    for (pth, dirs, files) in os.walk(dir):
        for fname in files:
            yield Path(pth, fname)


def parse_msi(prefixes, multiplier, msi_threshold, file, control_info):
    """
    Create msi output file with MSI location counts for multiple samples
    """

    total_loci = 0
    msi_loci = 0
    specimens = defaultdict(dict)
    loci_msi_analysis = defaultdict(dict)
    sample_msi_analysis = defaultdict(dict)

    if msi_threshold:
        threshold = msi_threshold
    else:
        threshold = [0.2, 0.2]

    # Parse the user defined thresholds:
    if len(threshold) == 1:
        min_thres = float(threshold)
        max_thres = float(threshold)
    elif len(threshold) == 2:
        min_thres = float(min(threshold))
        max_thres = float(max(threshold))
    else:
        sys.exit("Wrong number of -t thresholds given")

    with open(file) as fname:

        reader = csv.DictReader(fname, delimiter='\t', restkey='Misc')
        sample_msi = natsort.natsorted(reader, key=itemgetter('Position'))

        for key, group in groupby(sample_msi, key=itemgetter('Position')):

            control_row = [d for d in control_info if d['Position'] == key]

            try:
                variant = control_row[0]['Position']
            except IndexError:
                for sample_info in group:
                    sample_info['Loci_Status'] = str('None')
                    loci_msi_analysis[key] = sample_info
                continue

            for sample_info in group:
                if int(sample_info['Average_Depth']) >= 30:
                    threshold_value = float(control_row[0]['Average_Number_Peaks']) + (
                            multiplier * float(control_row[0]['Standard_Deviation']))
                    if int(sample_info['Number_of_Peaks']) >= threshold_value:
                        new_info = 1
                        sample_info['Loci_Status'] = str('MSI')
                    else:
                        new_info = 0
                        sample_info['Loci_Status'] = str('MSS')
                else:
                    new_info = None
                    sample_info['Loci_Status'] = str('None')

                specimens[variant] = new_info
                loci_msi_analysis[key] = sample_info

        for entry in specimens.items():
            if entry[1] is not None:
                total_loci += 1
                msi_loci = msi_loci + entry[1]

        sample_msi_analysis[prefixes]['Msi_Count'] = msi_loci
        sample_msi_analysis[prefixes]['Mss_Count'] = total_loci - msi_loci
        try:
            sample_msi_analysis[prefixes]['Msi_Score'] = "{0:.4f}".format(float(msi_loci) / total_loci)
            # POS if above or equal to max threshold
            if float(sample_msi_analysis[prefixes]['Msi_Score']) >= float(max_thres):
                sample_msi_analysis[prefixes]['Msi_Status'] = "POS"
            elif float(sample_msi_analysis[prefixes]['Msi_Score']) < float(min_thres):
                sample_msi_analysis[prefixes]['Msi_Status'] = "NEG"
            elif min_thres < float(sample_msi_analysis[prefixes]['Msi_Score']) < max_thres:
                sample_msi_analysis[prefixes]['Msi_Status'] = "IND"
        except ZeroDivisionError:
            sample_msi_analysis[prefixes]['Msi_Status'] = "NEG"

    return (loci_msi_analysis, sample_msi_analysis)


def do_summary(config):
    path = config['path']
    multiplier = config['multiplier']
    msi_threshold = config['msi_threshold']
    control_file = config['control_file']
    output = config['output']

    msi_summary_info = defaultdict(dict)
    msi_summary_output = os.path.join(output, 'msi_summary.txt')

    control_info = csv.DictReader(control_file, delimiter='\t')
    control_info = natsort.natsorted(control_info, key=itemgetter('Position'))
    # control_info = [d for d in control_info]

    files = filter(msi_file_finder, walker(path))
    files = sorted(files)

    for pth in files:
        prefixes = pth.fname.replace('.msi.txt', '')
        file_path = os.path.join(pth.dir, pth.fname)

        msi_loci_output = os.path.join(output, (prefixes + '_loci_analysis.txt'))
        msi_status_output = os.path.join(output, (prefixes + '_msi_analysis.txt'))

        msi_loci_info, msi_status_info = parse_msi(prefixes, multiplier, msi_threshold, file_path, control_info)
        msi_summary_info.update(msi_status_info)

        with open(msi_loci_output, 'w') as msi_loci_file:
            fieldnames = ['Position', 'Name', 'Average_Depth', 'Number_of_Peaks', 'Standard_Deviation',
                          'IndelLength:AlleleFraction:SupportingCalls', 'Loci_Status']
            writer = csv.DictWriter(msi_loci_file, fieldnames=fieldnames, extrasaction='ignore', delimiter='\t')
            writer.writeheader()
            for key, row in natsort.natsorted(msi_loci_info.items()):
                if 'Misc' in row.keys():
                    row['IndelLength:AlleleFraction:SupportingCalls'] = row[
                                                                        'IndelLength:AlleleFraction:SupportingCalls'] + ' ' + ' '.join(
                        row['Misc'])
                    del row['Misc']
                writer.writerow(dict(row, **{'Position': key}))

        with open(msi_status_output, 'w') as msi_status_file:
            msi_status_fieldnames = ['Name', 'Msi_Count', 'Mss_Count', 'Msi_Score', 'Msi_Status']
            msi_status_writer = csv.DictWriter(msi_status_file, fieldnames=msi_status_fieldnames, extrasaction='ignore',
                                               delimiter='\t')
            msi_status_writer.writeheader()
            for key, row in natsort.natsorted(msi_status_info.items()):
                msi_status_writer.writerow(dict(row, **{'Name': key}))

    if len(msi_summary_info.keys()) > 1:
        with open(msi_summary_output, 'w') as msi_summary_file:
            msi_status_fieldnames = ['Name', 'Msi_Count', 'Mss_Count', 'Msi_Score', 'Msi_Status']
            msi_status_writer = csv.DictWriter(msi_summary_file, fieldnames=msi_status_fieldnames,
                                               extrasaction='ignore',
                                               delimiter='\t')
            msi_status_writer.writeheader()
            for key, row in natsort.natsorted(msi_summary_info.items()):
                msi_status_writer.writerow(dict(row, **{'Name': key}))


















