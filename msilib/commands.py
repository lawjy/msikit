"""Command-line interface and corresponding API for msikit."""
# NB: argparse CLI definitions and API functions are interwoven:
#   "_cmd_*" handles I/O and arguments processing for the command
#   "do_*" runs the command's functionality as an API
import os
import re
import sys
import logging
import argparse
from . import workflow
from ._version import __version__
from utilities import utils


import warnings
warnings.filterwarnings('ignore', message="numpy.dtype size changed")
warnings.filterwarnings('ignore', message="numpy.ufunc size changed")


__all__ = []
TAR_DIR = os.path.dirname(os.path.realpath(__file__))
ROOT_DIR = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))


def public(fn):
    __all__.append(fn.__name__)
    return fn


AP = argparse.ArgumentParser(
    description="fusion detected, a command-line toolkit for biologic analysis.")
AP_subparsers = AP.add_subparsers(
    help="Sub-commands (use with -h for more info)")
# _____________________________________________________________________________

#1. format---------------------------------------------------------------------
do_format = public(workflow.do_format)

def _cmd_format(args):
    """ file will be reformated in interval format"""
    workflow.do_format(vars(args))

P_format = AP_subparsers.add_parser('format', help=_cmd_format.__doc__)

P_format.add_argument('-i','--bedfile',
                        type=argparse.FileType('rU'),
                        default=sys.stdin,
                        help='Path to tab delimited bed file of format chr start end')

P_format.add_argument('-o','--outfile',
                        type=argparse.FileType('w'),
                        help='Name of the output file')

P_format.add_argument('-a', '--lextends',
                          type=float,
                          default=0.5,
                          help="""Fraction for extending from target region [Default: %(default)s]""")

P_format.set_defaults(func=_cmd_format)




#2. counts --------------------------------------------------------------------------------------------------
do_counts = public(workflow.do_counts)

def _cmd_counts(args):
    """ calc reads counts """
    workflow.do_counts(vars(args))

P_counts = AP_subparsers.add_parser('counts', help=_cmd_counts.__doc__)

P_counts.add_argument('-b', '--bedfile',
                    type=argparse.FileType('rU'),  ##(处理文件读写。argparse模块提供了FileType factory, 句柄)
                    help='Path to tab delimited bed file of format chr start end')

P_counts.add_argument('-c', '--countsfile',
                    type=argparse.FileType('rU'),
                    help='Name of the output file')

P_counts.add_argument('-o', '--outfile',
                    type=argparse.FileType('w'),
                    help='Name of the output file')

P_counts.add_argument('-a', '--lextends',
                    type=float,
                    default=0.5,
                    help="""Average size of  target region extended to the left (results are approximate).
                  [Default: %(default)s]""")

P_counts.set_defaults(func=_cmd_counts)



#3. analyze --------------------------------------------------------------------------------------------------
do_analyze = public(workflow.do_analyze)

def _cmd_analyze(args):
    """ calc major tyep of repetition"""
    workflow.do_analyze(vars(args))

P_analyze = AP_subparsers.add_parser('analyze', help=_cmd_analyze.__doc__)
P_analyze.add_argument('msi_file',
                    type=argparse.FileType('rU'),
                    default=sys.stdin,
                    help='Path of the msi output ')
P_analyze.add_argument('bedfile',
                    type=argparse.FileType('rU'),
                    default=sys.stdin,
                    help='Path to tab delimited bed file of format chr start end')
P_analyze.add_argument('-o', '--outfile',
                    type=argparse.FileType('w'),
                    help='Name of the output file. msi.txt will be tacked to the end')
P_analyze.add_argument('-p', '--peak_fraction_cutoff',
                    default=0.05,
                    type=float,
                    help='Peak fraction cutoff. Default of 0.05')

P_analyze.set_defaults(func=_cmd_analyze)



#4. baseline --------------------------------------------------------------------------------------------------
do_baseline = public(workflow.do_baseline)

def _cmd_baseline(args):
    """ generate baseline """
    workflow.do_baseline(vars(args))

P_baseline = AP_subparsers.add_parser('baseline', help=_cmd_baseline.__doc__)

P_baseline.add_argument('path',
                    help='Path to analysis files')
P_baseline.add_argument('-o', '--outfile', type=argparse.FileType('w'),
                    default=sys.stdout,
                    help='Name of the output file')

P_baseline.set_defaults(func=_cmd_baseline)



#5. summary ------------------------------------------------------------------------------------------------------
do_summary = public(workflow.do_summary)

def _cmd_summary(args):
    """ summary"""
    workflow.do_summary(vars(args))

P_summary = AP_subparsers.add_parser('summary', help=_cmd_summary.__doc__)

P_summary.add_argument('control_file',
                    type=argparse.FileType('rU'),
                    default=sys.stdin,
                    help='Path to control file')
P_summary.add_argument('path',
                    help='Path to analysis files')
P_summary.add_argument('-m', '--multiplier',
                    default=3.0,
                    type=float,
                    help='STD cutoff multiplier, default of 3.0')
P_summary.add_argument('-t', '--msi_threshold',
                    nargs='+',
                    help='MSI score threshold or range of two thresholds, default of 0.2')
# parser.add_argument('-p', '--pipeline_manifest', type=argparse.FileType('rU'),
#                    help='Path to pipeline manifest, used for ordering output in UW pipeline specifically')
P_summary.add_argument('-o', '--output',
                    default=sys.stdout,
                    help='Name of the output ')
P_summary.set_defaults(func=_cmd_summary)




# version ---------------------------------------------------------------------
def print_version(_args):
    """Display this program's version."""
    print(__version__)

P_version = AP_subparsers.add_parser('version', help=print_version.__doc__)
P_version.set_defaults(func=print_version)

# _____________________________________________________________________________
# Shim for command-line execution

def parse_args(args=None):
    """Parse the command line."""
    return AP.parse_args(args=args)



