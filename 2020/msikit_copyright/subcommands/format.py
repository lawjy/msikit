"""
Usage: python3 format.py /path/to/bed/file -o /path/to/outfile
"""
import csv
import sys
import argparse
import natsort
from collections import defaultdict

def build_parser(parser):
    parser.add_argument('-i','--bedfile', 
                        type=argparse.FileType('rU'),
                        default=sys.stdin,
                        help='Path to tab delimited bed file of format chr start end')
    parser.add_argument('-o','--outfile', 
                        type=argparse.FileType('w'),
                        help='Name of the output file')
    parser.add_argument('-a', '--lextends',
                          type=float,
                          default=0.5,
                          help="""Fraction for extending from target region [Default: %(default)s]""")

def coords(row, chromosome=0, start=1, stop=2):
    return row[chromosome], int(row[start]), int(row[stop])

def msi_interval_creator(ranges):
    """Change the range information into varscan format
    chr start - T 
    """
    data=[]
    #make sure the chr is sorted
    for chr in natsort.natsorted(ranges.keys()):
        #make sure the start position is sorted
        for row in sorted(ranges[chr]):
            info=(chr, row, '-', 'T')
            data.append(info)
    return data

def action(args):
    ## instantiation
    bedfile = args.bedfile
    output=args.outfile
    # prepare a dictionary of chromosome: set(positions)
    # includes all positions between start-stop
    ranges = defaultdict(set)
    writer=csv.writer(output,delimiter='\t')
    for row in csv.reader(bedfile,delimiter='\t'):
        chrom, beg, end = coords(row)
        ranges[chrom].update(range(beg - int(args.lextends*(end - beg)), end + 1))
    writer.writerows(msi_interval_creator(ranges))

if __name__ == '__main__':
    action()




### input 
# 1	120053341	120053377	MSI_001
# 2	48032741	48032753	MSI_002
# 2	95849362	95849384	MSI_003


## mid-tmp
## ranges['1'] = set(120053323, 120053324, ...)


## output
# 1	120053323	-	T
# 1	120053324	-	T
# 1	120053325	-	T
# 1	120053326	-	T
# 1	120053327	-	T
# 1	120053328	-	T
# 1	120053329	-	T


### 知识点
# defaultdict(set) dict 嵌套 set (list)
# dict update()函数
# natsort 自然数排序


### 事件描述

