import glob
from os.path import splitext, split, join


def itermodules(subcommands_path, root=__name__):
    commands = [x for x in [splitext(split(p)[1])[0] for p in glob.glob(join(subcommands_path, '*.py'))] if not x.startswith('_')]

    for command in commands:
        yield command, __import__('%s.%s' % (root, command), fromlist=[command])
#
# from os.path import dirname, basename, isfile, join
# import glob
# modules = glob.glob(join(dirname(__file__), "*.py"))
# __all__ = [ basename(f)[:-3] for f in modules if isfile(f) and not f.endswith('__init__.py')]