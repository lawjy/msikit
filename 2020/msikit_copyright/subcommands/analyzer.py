"""
Usage: python3 analyzer.py msifile bedfile -o outfile
"""
#!/usr/bin/env python3
# _*_ coding:utf-8 _*_
import sys
import csv
import re
import argparse
import natsort
from numpy import std, array, ceil
import pprint

def build_parser(parser):
    parser.add_argument('msi_file',
                        type=argparse.FileType('rU'),
                        default=sys.stdin,
                        help='Path of the msi output ')
    parser.add_argument('bedfile',
                        type=argparse.FileType('rU'),
                        default=sys.stdin,
                        help='Path to tab delimited bed file of format chr start end')
    parser.add_argument('-o', '--outfile',
                        type=argparse.FileType('w'),
                        help='Name of the output file. msi.txt will be tacked to the end')
    parser.add_argument('-p', '--peak_fraction_cutoff',
                        default=0.05,
                        type=float,
                        help='Peak fraction cutoff. Default of 0.05')


def calc_highest_peak(info, wt_fraction, average_depth):
    """
    Calculate the highest peak
    """
    highest_frac = wt_fraction
    for loci, details in info.items():
        details['allele_fraction'] = float(details['mutant_depth']) / average_depth
        if details['allele_fraction'] >= highest_frac:
            highest_frac = details['allele_fraction']
    return highest_frac

def define_sites(indels, sites):
    """
    Create a site entry for all variant lengths between abs(max) and 0
    """
    # If only one indel
    indels = list(indels)
    if len(indels) == 1:
        # If del
        if indels[0] > 0:
            mx = int(indels[0])
            mn = 0
        # If ins
        else:
            mx = 0
            mn = int(indels[0])
    else:
        mn = int(min(indels))
        mx = int(max(indels))
        # If both dels
        if mn < 0 and mx < 0:
            mx = 0
        # if both ins
        elif mn > 0 and mx > 0:
            mn = 0
    sites[0] = '0:0:0'
    for key in range(mn, mx + 1):
        if key not in sites.keys():
            sites[key] = str(key) + ':0:0'
    return sites

def calc_wildtype(indels, wildtype_ave_depth, wildtype_fraction, highest_frac):
    sites = {}
    try:
        wt_frac = float(wildtype_fraction) / highest_frac
    except ZeroDivisionError:
        wt_frac = float(wildtype_fraction)

    wildtype = ':'.join(['0', str(wt_frac), str(wildtype_ave_depth)])
    sites[0] = wildtype
    return sites

def calc_number_peaks(info, sites, highest_frac, cutoff):
    """
    Calculate the number of peaks that are above cutoff
    """
    peaks = 0
    for loci, details in info.items():
        ## 重要的方法学
        # take highest allele fraction and divide each alele fraction by that number
        try:
            allele_fraction = float(details['allele_fraction']) / highest_frac
        except ZeroDivisionError:
            allele_fraction = float(details['allele_fraction'])

        # Overwrite this site if the allele fraction is more than the cutoff
        sites[loci] = ':'.join([str(loci), str(allele_fraction), str(details['mutant_depth'])])

        if allele_fraction >= cutoff and int(details['mutant_depth']) >=3:
            peaks += 1

    # wildtype is sites[0], if fraction is above cutoff, it counts as a peak
    if float(sites[0].split(':')[1]) >= cutoff and int(sites[0].split(':')[2]) >=3:
        peaks += 1
    return peaks, sites

def calc_std_peaks(peaks):
    """
    Calculate the standard deviation of the alleles
    """
    std_peaks = []
    for peak in peaks:
        allele = peak.split(':')
        for i in range(0, int(allele[2])):
            std_peaks.append(int(allele[0]))

    a = array(std_peaks)
    stddev = format(std(a), '.6f')
    return stddev

def calc_summary_stats(output_info, cutoff):
    sites = {}
    # msi_info is all loci for this chromosome
    for name, info in output_info.items():
        if info['total_depth'] != 0 and info['total_sites'] != 0:
            # Use ceil to round up
            average_depth = ceil(float(info['total_depth']) / info['total_sites'])
            # Turn to int
            average_depth = int(average_depth)
        else:
            average_depth = 0

        if average_depth != 0 and info['total_mutant_depth'] <= average_depth:
            wildtype_ave_depth = int(average_depth - info['total_mutant_depth'])
            wildtype_fraction = float(wildtype_ave_depth) / average_depth
        else:
            wildtype_ave_depth = 0
            wildtype_fraction = 0
            sites[0] = '0:0:0'
        if info['indels']:
            highest_frac = calc_highest_peak(info['indels'], wildtype_fraction, average_depth)
            sites = calc_wildtype(info['indels'].keys(), wildtype_ave_depth, wildtype_fraction, highest_frac)

            num_peaks, peaks = calc_number_peaks(info['indels'], sites, highest_frac, cutoff)
            stdev = calc_std_peaks(peaks.values())
            # Sort the peak list naturally (-3,-2,-1,0,1,2,3)
            peak_list = (" ").join(str(x) for x in natsort.natsorted(peaks.values()))
        elif average_depth != 0:
            wildtype_fraction = 1
            stdev = 0
            num_peaks = 1
            sites[0] = (':').join(['0', str(float(wildtype_fraction)), str(wildtype_ave_depth)])
            peak_list = sites[0]
        else:
            wildtype_fraction = 0
            sites[0] = (":").join(['0', str(float(wildtype_fraction)), str(wildtype_ave_depth)])
            num_peaks = 0
            peak_list = sites[0]
            stdev = 0
        output_info[name] = {'Name': info['Name'],
                             'Average_Depth': average_depth,
                             'Standard_Deviation': stdev,
                             'Number_of_Peaks': num_peaks,
                             'IndelLength:AlleleFraction:SupportingCalls': peak_list}
    return output_info

def calc_msi_dist(variant, msi_sites):
    msi_sites['total_depth'] = int(msi_sites['total_depth']) + int(variant['depth'])
    msi_sites['total_sites'] = int(msi_sites['total_sites']) + 1
    msi_sites['site_depth'] = int(variant['depth'])  

    # Grab all the deletions
    dels = filter(lambda s: 'DEL' in str(s), variant['Misc'])
    # And all the insertions
    ins = filter(lambda s: 'INS' in str(s), variant['Misc'])
    sites = list(dels) + list(ins)
    # Now process DEL/INS specific info
    for entry in sites:
        info = re.split('-|:', entry)
        if info[0] == 'DEL':
            length = int(info[1]) * int(-1)
        elif info[0] == 'INS':
            length = int(info[1])
        reads = info[3]
        # Want a total mutant depth for this loci
        msi_sites['total_mutant_depth'] += int(reads)
        msi_sites['mutant_tally'] += 1
        if length == 0:
            continue
        # If we haven't seen this length of mutation, add it
        if not length in msi_sites['indels'].keys():
            msi_sites['indels'][length] = {'allele_fraction': 0}
            msi_sites['indels'][length]['site_depth'] = int(variant['depth'])
            msi_sites['indels'][length]['mutant_depth'] = int(reads)
            msi_sites['indels'][length]['mutant_tally'] = 1
            # Otherwise, update the counts for this mutation length
        else:
            msi_sites['indels'][length]['site_depth'] += int(variant['depth'])
            msi_sites['indels'][length]['mutant_depth'] += int(reads)
            msi_sites['indels'][length]['mutant_tally'] += 1

    return msi_sites

def parse_msi_bedfile(row, msi_sites, output_info):
    msi_loci = row['chrom'] + ':' + row['start'] + '-' + row['end']
    if not row['chrom'] in msi_sites.keys():
        msi_sites[row['chrom']] = {}
    for position in range(int(row['start']), int(row['end']) + 1):
        msi_sites[row['chrom']][position] = msi_loci
    output_info[msi_loci] = {'Name': row['name'],
                             'total_depth': 0,
                             'total_sites': 0,
                             'total_mutant_depth': 0,
                             'mutant_tally': 0,
                             'indels': {}}
    return (msi_sites, output_info)

def action(args):

    msifile = args.msi_file
    bedfile = args.bedfile
    outfile = args.outfile
    cutoff = args.peak_fraction_cutoff
    msi_sites = {}
    output_info = {}
    output = {}

    bedfile_handle = csv.DictReader(bedfile, delimiter='\t', fieldnames=['chrom', 'start', 'end', 'name'])
    for row in bedfile_handle:
        msi_sites, output_info = parse_msi_bedfile(dict(row), msi_sites, output_info)
    # now we start processing the sample msi info
    sample_msi = csv.DictReader(msifile, delimiter='\t', restkey='Misc')
    # Evaluate the sample-info vs msi-bed info, grouping on chromosome.
    for row in sample_msi:
        loci_name = msi_sites[row['chrom']][int(row['position'])]
        output_info[loci_name].update(calc_msi_dist(row, output_info[loci_name]))

    output.update(calc_summary_stats(output_info, cutoff))
    fieldnames = ['Position', 'Name', 'Average_Depth', 'Number_of_Peaks', 'Standard_Deviation',
                  'IndelLength:AlleleFraction:SupportingCalls']
    writer = csv.DictWriter(outfile, fieldnames=fieldnames, extrasaction='ignore', delimiter='\t')
    writer.writeheader()
    for key, row in natsort.natsorted(output.items()):
        writer.writerow(dict(row, **{'Position': key}))

if __name__ == '__main__':
    main()
