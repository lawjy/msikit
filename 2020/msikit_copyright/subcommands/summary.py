"""
    summary 
"""

#!/usr/bin/env python3
# _*_ coding:utf-8 _*

import os
import csv
import sys
import natsort
import argparse
from operator import itemgetter
from collections import defaultdict, namedtuple
from itertools import groupby

def build_parser(parser):

    parser.add_argument('control_file',
                        type=argparse.FileType('rU'),
                        default=sys.stdin,
                        help='Path to control file')
    parser.add_argument('path',
                        help='Path to analysis files')
    parser.add_argument('-m', '--multiplier',
                        default=3.0,
                        type=float,
                        help='STD cutoff multiplier, default of 3.0')
    parser.add_argument('-t', '--msi_threshold',
                        nargs='+',
                        help='MSI score threshold or range of two thresholds, default of 0.2')
    parser.add_argument('-o', '--output',
                        default=sys.stdout,
                        help='Name of the output ')


def msi_file_finder(pth):
    """
    Return True if pth represents a msi file file.
    """
    return bool(pth.fname.endswith('.msi.txt'))

def walker(dir):
    """Recursively traverse direcory `dir`. For each tuple containing
    (path, dirs, files) return a named tuple with attributes (dir,
    fname) for each file name in `files`.
    """
    Path = namedtuple('Path', ['dir', 'fname'])
    for (pth, dirs, files) in os.walk(dir):
        for fname in files:
            yield Path(pth, fname)

def parse_msi(prefixes, multiplier, msi_threshold, file, control_info):
    """
    Create msi output file with MSI location counts for multiple samples
    """
    total_loci = 0
    msi_loci = 0
    specimens = defaultdict(dict)
    loci_msi_analysis = defaultdict(dict)
    sample_msi_analysis = defaultdict(dict)
    if msi_threshold:
        threshold = msi_threshold
    else:
        threshold = [0.2, 0.2]
    # Parse the user defined thresholds:
    if len(threshold) == 1:
        min_thres = float(threshold)
        max_thres = float(threshold)
    elif len(threshold) == 2:
        min_thres = float(min(threshold))
        max_thres = float(max(threshold))
    else:
        sys.exit("Wrong number of -t thresholds given")
    with open(file) as fname:
        reader = csv.DictReader(fname, delimiter='\t', restkey='Misc')
        sample_msi = natsort.natsorted(reader, key=itemgetter('Position'))
        for key, group in groupby(sample_msi, key=itemgetter('Position')):
            control_row = [d for d in control_info if d['Position'] == key]
            try:
                variant = control_row[0]['Position']
            except IndexError:
                for sample_info in group:
                    sample_info['Loci_Status'] = str('None')
                    loci_msi_analysis[key] = sample_info
                continue
            for sample_info in group:
                if int(sample_info['Average_Depth']) >= 30:
                    threshold_value = float(control_row[0]['Average_Number_Peaks']) + (
                            multiplier * float(control_row[0]['Standard_Deviation']))
                    if int(sample_info['Number_of_Peaks']) >= threshold_value:
                        new_info = 1
                        sample_info['Loci_Status'] = str('MSI')
                    else:
                        new_info = 0
                        sample_info['Loci_Status'] = str('MSS')
                else:
                    new_info = None
                    sample_info['Loci_Status'] = str('None')

                specimens[variant] = new_info
                loci_msi_analysis[key] = sample_info

        for entry in specimens.items():
            if entry[1] is not None:
                total_loci += 1
                msi_loci = msi_loci + entry[1]
        sample_msi_analysis[prefixes]['Msi_Count'] = msi_loci
        sample_msi_analysis[prefixes]['Mss_Count'] = total_loci - msi_loci
        try:
            sample_msi_analysis[prefixes]['Msi_Score'] = "{0:.4f}".format(float(msi_loci) / total_loci)
            # POS if above or equal to max threshold
            if float(sample_msi_analysis[prefixes]['Msi_Score']) >= float(max_thres):
                sample_msi_analysis[prefixes]['Msi_Status'] = "POS"
            elif float(sample_msi_analysis[prefixes]['Msi_Score']) < float(min_thres):
                sample_msi_analysis[prefixes]['Msi_Status'] = "NEG"
            elif min_thres < float(sample_msi_analysis[prefixes]['Msi_Score']) < max_thres:
                sample_msi_analysis[prefixes]['Msi_Status'] = "IND"
        except ZeroDivisionError:
            sample_msi_analysis[prefixes]['Msi_Status'] = "NEG"

    return (loci_msi_analysis, sample_msi_analysis)

def action(args):
    path = args.path
    multiplier = args.multiplier
    msi_threshold = args.msi_threshold
    control_file = args.control_file
    output = args.output
    msi_summary_info = defaultdict(dict)
    msi_summary_output = os.path.join(output, 'msi_summary.txt')
    control_info = csv.DictReader(control_file, delimiter='\t')
    control_info = natsort.natsorted(control_info, key=itemgetter('Position'))
    files = filter(msi_file_finder, walker(path))
    files = sorted(files)
    for pth in files:
        prefixes = pth.fname.replace('.msi.txt', '')
        file_path = os.path.join(pth.dir, pth.fname)
        msi_loci_output = os.path.join(output, (prefixes + '_loci_analysis.txt'))
        msi_status_output = os.path.join(output, (prefixes + '_msi_analysis.txt'))
        msi_loci_info, msi_status_info = parse_msi(prefixes, multiplier, msi_threshold, file_path, control_info)
        msi_summary_info.update(msi_status_info)
        with open(msi_loci_output, 'w') as msi_loci_file:
            fieldnames = ['Position', 'Name', 'Average_Depth', 'Number_of_Peaks', 'Standard_Deviation',
                          'IndelLength:AlleleFraction:SupportingCalls', 'Loci_Status']
            writer = csv.DictWriter(msi_loci_file, fieldnames=fieldnames, extrasaction='ignore', delimiter='\t')
            writer.writeheader()
            for key, row in natsort.natsorted(msi_loci_info.items()):
                if 'Misc' in row.keys():
                    row['IndelLength:AlleleFraction:SupportingCalls'] = row[
                                                                        'IndelLength:AlleleFraction:SupportingCalls'] + ' ' + ' '.join(
                        row['Misc'])
                    del row['Misc']
                writer.writerow(dict(row, **{'Position': key}))
        with open(msi_status_output, 'w') as msi_status_file:
            msi_status_fieldnames = ['Name', 'Msi_Count', 'Mss_Count', 'Msi_Score', 'Msi_Status']
            msi_status_writer = csv.DictWriter(msi_status_file, fieldnames=msi_status_fieldnames, extrasaction='ignore',
                                               delimiter='\t')
            msi_status_writer.writeheader()
            for key, row in natsort.natsorted(msi_status_info.items()):
                msi_status_writer.writerow(dict(row, **{'Name': key}))
    if len(msi_summary_info.keys()) > 1:
        with open(msi_summary_output, 'w') as msi_summary_file:
            msi_status_fieldnames = ['Name', 'Msi_Count', 'Mss_Count', 'Msi_Score', 'Msi_Status']
            msi_status_writer = csv.DictWriter(msi_summary_file, fieldnames=msi_status_fieldnames,
                                               extrasaction='ignore',
                                               delimiter='\t')
            msi_status_writer.writeheader()
            for key, row in natsort.natsorted(msi_summary_info.items()):
                msi_status_writer.writerow(dict(row, **{'Name': key}))

if __name__ == '__main__':
    main()

