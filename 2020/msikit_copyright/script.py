#!/usr/bin/env python3
# _*_ coding:utf-8 _*
"""
ngskit.py : Utilities for the NGS pipeline scripts
"""
import argparse
from argparse import RawDescriptionHelpFormatter
import sys
import os
import subcommands


def parse_arguments(argv):
    """
    Create the argument parser
    """
    version = '1.0.0'
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('-V', '--version', action='version',
        version = version,
        help = 'Print the version number and exit')
    parser.add_argument('-v', '--verbose',
        action='count', dest='verbosity', default=1,
        help='Increase verbosity of screen output (eg, -v is verbose, '
             '-vv more so)')
    parser.add_argument('-q', '--quiet',
        action='store_const', dest='verbosity', const=0,
        help='Suppress output')
    if len(argv) == 0:
        parser.print_help()
        sys.exit(1)
    # Setup all sub-commands
    subparsers = parser.add_subparsers(dest='subparser_name')
    # Begin help sub-command
    parser_help = subparsers.add_parser(
        'help', help='Detailed help for actions using `help <action>`')
    parser_help.add_argument('action', nargs=1)
    # End help sub-command
    actions = {}
    for name, mod in subcommands.itermodules(os.path.split(subcommands.__file__)[0]):
        subparser = subparsers.add_parser(name,
                                          help = mod.__doc__.lstrip().split('\n', 1)[0],
                                          description = mod.__doc__,
                                          formatter_class = RawDescriptionHelpFormatter)
        mod.build_parser(subparser)
        actions[name] = mod.action
    arguments = parser.parse_args(argv)
    # Determine which action is in play.
    action = arguments.subparser_name
    # Support help <action> by simply having this function call itself and translate the arguments into something that argparse can work with.
    if action == 'help':
        return parse_arguments([str(arguments.action[0]), '-h'])

    return actions[action], arguments

def main(argv):
    action, arguments = parse_arguments(argv)
    return action(arguments)
