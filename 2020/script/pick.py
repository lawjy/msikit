import sys
import re
import csv
import pprint


def filter(seq_name, sam):
    with open(sam, 'r') as fname:
        spamreader = csv.reader(fname, delimiter='\t')
        for row in spamreader:
            if row[0] in seq_name:
                yield row


def main():
    sam = sys.argv[1]
    qname = sys.argv[2]
    output = sys.argv[3]

    seq_name = []
    with open(qname, 'r') as fname:
        for line in fname.readlines():
            seq_name +=re.split(',', line.strip())

    seq_name = list(set(seq_name))



    with open(output, 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter='\t', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        for info in filter(seq_name, sam):
            spamwriter.writerow(info)
        pass


if __name__ == '__main__':
    main()
