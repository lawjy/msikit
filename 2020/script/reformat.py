#!/usr/bin/env python3
# _*_ coding:utf-8 _*

import re
import csv
import natsort
import argparse
import pprint


def build_parser():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)   ## 创建一个ArgumentParse对象

    ## 添加参数
    parser.add_argument('-b', '--bedfile',
                        type=argparse.FileType('rU'),   ##(处理文件读写。argparse模块提供了FileType factory, 句柄)
                        help='Path to tab delimited bed file of format chr start end')

    parser.add_argument('-c', '--countsfile',
                        type=argparse.FileType('rU'),
                        help='Name of the output file')

    parser.add_argument('-o', '--outfile',
                        type=argparse.FileType('w'),
                        help='Name of the output file')

    parser.add_argument('-a', '--lextends',
                          type=float,
                          default=0.5,
                          help="""Average size of  target region extended to the left (results are approximate).
                    [Default: %(default)s]""")
    ## 解析参数
    args = parser.parse_args()
    return args

def parser_msi_bed(frc, row, msi_site, output_info):

    if not row['chrom'] in msi_site.keys():
        msi_site[row['chrom']] = {}

    for pos in range(int(int(row['start']) - (int(row['end'])-int(row['start']))*frc), int(row['end'])+1):
        msi_site[row['chrom']][pos] = row['start']+'_'+row['end']

    for pos in range(int(row['start']), int(row['end'])+1):
        msi_loci = row['chrom']+'_'+str(pos)
        output_info[msi_loci] = {
                        'chrom': row['chrom'],
                        'position': pos,
                        'ref_base': 'N',
                        'depth': 0,
                        'q10_depth': 0,
                        'base:reads:strands:avg_qual:map_qual:plus_reads:minus_reads': 'N:0:0:0:0:0:0:0',
                        'Misc': []
                    }

    return msi_site, output_info


def calc_real_counts(detail, adjust_loci, output_info, stype=None):

    ### cacl region of extension
    length = adjust_loci - int(detail['position']) - 1

    ### cacl variation
    dels = filter(lambda s: 'DEL' in str(s), detail['Misc'])
    ins = filter(lambda s: 'INS' in str(s), detail['Misc'])
    sites = list(dels) + list(ins)

    for site in sites:
        info = re.split('[-:]', site)
        if info[0] == 'DEL' and int(info[1]) > length:
            if stype is not None:
                info[1] = length + 1
                info[2] = info[2][0:length + 1]
                if info[1] == 0:
                    continue
            else:
                info[1] = int(info[1]) - length
                info[2] = info[2][length:]

            output_info['Misc'].append(':'.join(['-'.join([info[0],str(info[1]),info[2]]), info[3],info[4],info[5],info[6],info[7],info[8]]))

        if info[0] == 'INS' and length == 0 and len(set(info[2])) == 1 and stype is None:
            output_info['Misc'].append(site)

    ## update info
    output_info.update({
                        'chrom':detail['chrom'],
                        'position':detail['position'],
                        'ref_base':detail['ref_base'],
                        'depth':detail['depth'],
                        'q10_depth':detail['q10_depth'],
                        'base:reads:strands:avg_qual:map_qual:plus_reads:minus_reads':detail['base:reads:strands:avg_qual:map_qual:plus_reads:minus_reads'],
                    })

    return output_info


def action():
    args = build_parser()   ## 实例化参数

    msi_site = {}
    output_info = {}

    ## 读取 bed 文件
    for row in csv.DictReader(args.bedfile, fieldnames=['chrom', 'start', 'end', 'name'], restkey='Misc', delimiter='\t'):
        msi_site, output_info = parser_msi_bed(args.lextends, row, msi_site, output_info)


    # ## 读取readscount 文件并处理
    for row in csv.DictReader(args.countsfile, delimiter='\t', restkey='Misc'):
        if int(row['position']) not in msi_site[row['chrom']].keys():
            continue

        start_loci = int(re.split(r'_', msi_site[row['chrom']][int(row['position'])])[0])
        end_loci = int(re.split(r'_', msi_site[row['chrom']][int(row['position'])])[1])

        if int(row['position']) <= start_loci:
            real_loci = row['chrom']+'_'+str(start_loci)
            output_info[real_loci].update(calc_real_counts(row, start_loci, output_info[real_loci]))

        elif start_loci < int(row['position']) and int(row['position']) <=  end_loci:
            real_loci = row['chrom'] + '_' + str(row['position'])
            output_info[real_loci].update(calc_real_counts(row, end_loci, output_info[real_loci], 1))

    fieldnames = ['chrom', 'position', 'ref_base', 'depth', 'q10_depth', 'base:reads:strands:avg_qual:map_qual:plus_reads:minus_reads']
    writer = csv.writer(args.outfile, delimiter='\t')
    writer.writerow(fieldnames)

    for key, row in natsort.natsorted(output_info.items()):
        chrom_pos = re.split('_', key)
        output = [chrom_pos[0], chrom_pos[1], row['ref_base'], row['depth'], row['q10_depth'], row['base:reads:strands:avg_qual:map_qual:plus_reads:minus_reads'], '0']
        if row['Misc']:
            for info in row['Misc']:
                output.append(info)
        writer.writerow(output)


if __name__ == '__main__':
    action()
