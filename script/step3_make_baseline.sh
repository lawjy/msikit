#!/bin/bash


root='/data/IndividualArchive/luojy/msikit/script/'
### Baseline will be generated, which data source from normal sample

VARSCAN=/data/IndividualArchive/luojy/msikit/bin/VarScan.v2.3.7.jar
samtools=/data/IndividualArchive/luojy/msikit/bin/samtools/bin/samtools


## databases
REF_GENOME=/data/IndividualArchive/luojy/ngskit/db/Homo_sapiens_assembly19.fasta
INTERVALS_FILE=/data/IndividualArchive/luojy/msikit/database/msi.intervals2
BEDFILE=/data/IndividualArchive/luojy/msikit/database/msi.bed


#BAM_LIST is a file of absolute paths to each bam file

BAM_LIST=$1
SAVEPATH=$2



for BAM in `sed '/^$/d' $BAM_LIST`; do
    BAMPATH=$(dirname $BAM)
    BAMNAME=$(basename $BAM)
    PFX=${BAMNAME%.*}

    mkdir -p $SAVEPATH/$PFX

    echo “Starting Analysis of $PFX” >> $SAVEPATH/msi_run_log.txt;
    date +"%D %H:%M" >> $SAVEPATH/msi_run_log.txt;

    echo "Making mpileups" >> $SAVEPATH/msi_run_log.txt;
    date +"%D %H:%M" >> $SAVEPATH/msi_run_log.txt;
    $samtools mpileup -f $REF_GENOME -d 100000 -A -E $BAM -l $INTERVALS_FILE -Q 0 -C 50  --output-QNAME | awk '{if($4 >= 6) print $0}' > $SAVEPATH/$PFX/$PFX.mpileup 
    
    echo "Varscan Readcounts start" >> $SAVEPATH/msi_run_log.txt;
    date +"%D %H:%M" >> $SAVEPATH/msi_run_log.txt;
    java -Xmx4g -jar $VARSCAN readcounts $SAVEPATH/$PFX/$PFX.mpileup --variants-file $INTERVALS_FILE --min-base-qual 10 --output-file $SAVEPATH/$PFX/$PFX.msi_output &
    wait

    echo "MSI Analyzer start" >> $SAVEPATH/msi_run_log.txt;
    date +"%D %H:%M" >> $SAVEPATH/msi_run_log.txt;
    python3 $root/reformat.py -b $BEDFILE -c $SAVEPATH/$PFX/$PFX.msi_output -o $SAVEPATH/$PFX/$PFX.msi_output_re
    python3 $root/analyzer.py $SAVEPATH/$PFX/$PFX.msi_output_re $BEDFILE -o $SAVEPATH/$PFX/$PFX.msi.txt

    awk '{print $7}' $SAVEPATH/$PFX/$PFX.mpileup  > $SAVEPATH/$PFX/$PFX.qname 
    python3 /data/IndividualArchive/luojy/msikit/script/pick.py $BAMPATH/$PFX.sam $SAVEPATH/$PFX/$PFX.qname $SAVEPATH/$PFX/$PFX.pick.sam
    $samtools view -bS $SAVEPATH/$PFX/$PFX.pick.sam -T $REF_GENOME -o $SAVEPATH/$PFX/$PFX.pick.bam
    $samtools index $SAVEPATH/$PFX/$PFX.pick.bam

done

echo "Creating baseline of all files" >> $SAVEPATH/msi_run_log.txt;
date +"%D %H:%M" >> $SAVEPATH/msi_run_log.txt;

#msi create_baseline $SAVEPATH -o $SAVEPATH/MSI_BASELINE.txt
python3 $root/create_baseline.py $SAVEPATH -o $SAVEPATH/MSI_BASELINE.txt

echo “Completed Analysis of $PFX” >> $SAVEPATH/msi_run_log.txt;
date +"%D %H:%M" >> $SAVEPATH/msi_run_log.txt;

exit

