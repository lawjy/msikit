bamlist=$1
output=$2
samtools="/data/IndividualArchive/luojy/msikit/bin/samtools/bin/samtools"
bin='/data/IndividualArchive/luojy/ngskit/bin/bedtools/bin'
db='/data/IndividualArchive/luojy/msikit/database'

cd $output
for i in `less -S $bamlist`;do

    sampleid=`basename $i | cut -d. -f 1`
    ${bin}/bedtools intersect -a $i -b ${db}/msi_for_trim.1.bed > ${sampleid}.tmp && \
    ${bin}/bedtools intersect -a ${sampleid}.tmp -b ${db}/msi_for_trim.2.bed > ${sampleid}.trim.bam && \
    $samtools view ${sampleid}.trim.bam > ${sampleid}.trim.sam && \
    $samtools index ${sampleid}.trim.bam
	

done

rm *.tmp
