#! /usr/bin/sh 

#sample_rbam=$1
sample_cbam=$1
output=$2
samtools="/data/IndividualArchive/luojy/msikit/bin/samtools/bin/samtools"
bedtools="/data/IndividualArchive/luojy/ngskit/bin/bedtools/bin/bedtools"
refGenome="/data/IndividualArchive/luojy/ngskit/db/Homo_sapiens_assembly19.fasta"
thread=4



cd $output
sample_mbam=`basename ${sample_cbam} |sed 's/bam/mbam/'`

$samtools view -@ $thread -q 13 -bF 4 $sample_cbam -o _msi_consolidated.bam
$bedtools bamtobed -cigar -i _msi_consolidated.bam | awk '{print $4}' > _msi_consolidated.id

$samtools view  _msi_consolidated.bam > _msi_consolidated.sam
paste  _msi_consolidated.id _msi_consolidated.sam | cut -f1,3- > msi_consolidated.sam

$samtools view -T $refGenome -bS msi_consolidated.sam -o msi_consolidated.bam
$samtools sort -@ $thread msi_consolidated.bam -o $sample_mbam
$samtools index $sample_mbam
rm *msi*



#
