import os
import re
from collections import defaultdict, namedtuple

def msi_file_finder(pth):
    """
    Return True if pth represents an analysis file.
    """
    return bool(re.search(r'.msi.txt', pth.fname))


def walker(dir):
    """Recursively traverse direcory `dir`. For each tuple containing
    (path, dirs, files) return a named tuple with attributes (dir,
    fname) for each file name in `files`.
    """
    Path = namedtuple('Path', ['dir', 'fname'])
    for (pth, dirs, files) in os.walk(dir):
        for fname in files:
            yield Path(pth, fname)